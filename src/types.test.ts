import * as types from "./index";

const tileTypeMock = "castle" as types.TileType;

const tileMock = {
  reference: "some_uniq_ref",
  image_url: "http://some_url.org/tileimage",
  top: "road" as types.TileType,
  bottom: "castle" as types.TileType,
  left: "road" as types.TileType,
  right: "feeld" as types.TileType,
  center: "feeld" as types.TileType,
  connections: [["top" as types.Positions, "left" as types.Positions]],
};

const userMock = {
  reference: "some reference",
};

const coordsMock = {
  x: 100,
  y: 100,
};

const gameMeepleMock = {
  position: "top" as types.Positions,
  onPosition: true,
};

const gameTileMock = {
  ...tileMock,
  gameTilePosition: coordsMock,
  rotation: 1,
  setTime: new Date(),
  availableFrom: new Date(),
  owner: userMock,
  meeples: [gameMeepleMock],
};

const gameUserMock = {
  ...userMock,
  points: 100,
  gameTiles: [gameTileMock as types.GameTile],
  meeples: [gameMeepleMock as types.GameMeeple],
};

const gameMock = {
  width: 20,
  height: 20,
  gameTiles: { 100: gameTileMock as types.GameTile },
  gameUsers: [gameUserMock as types.GameUser],
};

describe("Проверяем, что все нужные типы и интерфейсы экспортируются как надо", () => {
  test("Тайлы", () => {
    const tileType: types.TileType = tileTypeMock;
    const tile: types.Tile = { ...tileMock };
    expect(tileType === tileTypeMock);
    expect(tile).toEqual(tileMock);
  });

  test("Пользователь", () => {
    const user: types.User = { ...userMock };
    expect(user).toEqual(userMock);
  });

  test("Игра", () => {
    const gameTile: types.GameTile = { ...gameTileMock };
    const gameMeeple: types.GameMeeple = { ...gameMeepleMock };
    const gameUser: types.GameUser = { ...gameUserMock };
    const game: types.Game = { ...gameMock };
    expect(gameMeeple).toEqual(gameMeepleMock);
    expect(gameTile).toEqual(gameTileMock);
    expect(gameUser).toEqual(gameUserMock);
    expect(game).toEqual(gameMock);
  });
});
