export type TileType = "castle" | "road" | "field" | "river" | "monastery";
export type Positions = "top" | "bottom" | "left" | "right" | "center";

export interface Tile {
  // Уникальный идентификатор тайла
  reference: string;

  // URL картинки - оставим здесь
  image_url: string;

  // Тип частей тайла
  top: TileType;
  bottom: TileType;
  left: TileType;
  right: TileType;
  center: TileType;

  // Соединения частей тайла
  connections: Positions[][];
}
