import { Tile, Positions } from "./tile";
import { User } from "./user";

export interface Coords {
  x: number;
  y: number;
}

export interface GameMeeple {
  position: Positions;
  onPosition: boolean;
}

export interface GameTile extends Tile {
  // Позиция тайла на поле
  gameTilePosition: Coords;

  // Поворот тайла - 0 не повернут, 1 - один поворот по часовой стрелке, 2 - два поворота и т.д.
  rotation: number;

  // Время установки
  setTime: Date;

  // Когда тайл станет доступен для установки
  availableFrom: Date;

  // Владелец
  owner: User;

  // Установленные на тайл миплы
  meeples: GameMeeple[];
}

export interface GameUser extends User {
  // Очки пользователя в игре
  points: number;

  // Установленные пользователем тайлы
  gameTiles: GameTile[];

  // Миплы пользователя - и установленные и снятые идентификатор
  // - порядковый номер тайла считается так id = y * height + width
  meeples: { [id: number]: GameMeeple };
}

export interface Game {
  // Длина и ширина поля
  width: number;
  height: number;

  // Идентификатор - порядковый номер тайла считается так id = y * height + width,
  //  для того чтобы не создавать массив с пустыми элементами храним тайлы в объекте
  gameTiles: { [id: number]: GameTile };

  // Пользователи игры
  gameUsers: GameUser[];
}
