export interface User {
  // Идентификатор пользователя - должен быть уникальным для каждого пользователя
  reference: string;
}
