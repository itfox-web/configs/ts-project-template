# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.4.4](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.4.3...v1.4.4) (2020-06-19)

### Bug Fixes

- Исправлен пример манифеста gitlab ci ([9322278](https://gitlab.com/itfox-web/configs/ts-project-template/commit/93222789193def382be8a06819978cbec76a2ef2))

### [1.4.3](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.4.2...v1.4.3) (2020-06-19)

### Bug Fixes

- Добавлено указание на внешний конфиг prettier ([213a42f](https://gitlab.com/itfox-web/configs/ts-project-template/commit/213a42f746ef1a0e97d1fcc05ad8916e1974397f))

### [1.4.2](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.4.1...v1.4.2) (2020-06-18)

### [1.4.1](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.4.0...v1.4.1) (2020-06-18)

## [1.4.0](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.3.1...v1.4.0) (2020-06-18)

### Features

- Добавлен пример манифеста gitlab ci ([b1490ab](https://gitlab.com/itfox-web/configs/ts-project-template/commit/b1490ab2382ed6164013b68c1f3406e2ccb6721f))

### [1.3.1](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.3.0...v1.3.1) (2020-06-18)

### Bug Fixes

- Исправлено описание мажорного релиза ([ec74197](https://gitlab.com/itfox-web/configs/ts-project-template/commit/ec74197c9603be600e2b4f6cb9580279fe9385eb))

## [1.3.0](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.2.0...v1.3.0) (2020-06-18)

### Features

- Добавлено автоматическое присвоение ветке upstream ([22de366](https://gitlab.com/itfox-web/configs/ts-project-template/commit/22de366daf75bc38eed1cacba744b12e381ec721))

## [1.2.0](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.1.0...v1.2.0) (2020-06-18)

### Features

- Добавлены скрипты релиза и их описание ([30386f9](https://gitlab.com/itfox-web/configs/ts-project-template/commit/30386f971e82f5a8ae007817e538dfd2ac6e70bf))

## [1.1.0](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v1.0.0...v1.1.0) (2020-06-18)

## [1.0.0](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v0.1.1...v1.0.0) (2020-06-18)

### [0.1.1](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v0.1.0...v0.1.1) (2020-06-18)

## [0.1.0](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v0.0.4...v0.1.0) (2020-06-18)

### [0.0.4](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v0.0.3...v0.0.4) (2020-06-18)

### [0.0.3](https://gitlab.com/itfox-web/configs/ts-project-template/compare/v0.0.2...v0.0.3) (2020-06-18)

### Features

- Добавлено описание мезанизма коммитов ([150be80](https://gitlab.com/itfox-web/configs/ts-project-template/commit/150be80050ea013e2274637e5ec72e7d404c4905))

### 0.0.2 (2020-06-18)

### Bug Fixes

- Добавлено описание пакетов ([3edf550](https://gitlab.com/itfox-web/configs/ts-project-template/commit/3edf5504ab4ad46751b66b155955de611016baba))
- Добавлены основные настройки ([75b1059](https://gitlab.com/itfox-web/configs/ts-project-template/commit/75b1059d80a0263a095a3af2627cf7237cbf491e))
