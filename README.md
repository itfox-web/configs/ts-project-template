# TS project template

Шаблон для старта ts проекта

# main

typescript

# git

@commitlint/cli - проверка шаблонов сообщений коммитов
@commitlint/config-conventional -
husky - скрипты вызываемые во время работы с гит
standard-version - версионирование и формирование логов разработки

Перед работой обязательно установить:
`git config --global push.default current`

Создание коммита:
`git commit -m "feat: текст коммита"`

Сообщение дожно быть именно в таком формате: `feat: текст коммита` или `fix: текст коммита`
`feat:` - новый функционал
`fix:` - исправление

Коммиты попадают в CHANGELOG.md для удобства тестирования

Пререлиз релиза 0.2.0 -> 0.2.1:
`npm run release`

Создание минорного релиза 0.2.0 -> 0.3.0:
`npm run minor-release`

Создание мажорного релиза 0.2.0 -> 1.0.0:
`npm run minor-release`

# lint

tslint - type script линтер
tslint-config-prettier - разрешение конфликтов между приттиером и линтером
tslint-plugin-prettier - разрешение конфликтов между приттиером и линтером
prettier - оформление кода
pretty-quick - быстрое форматирование кода перед коммитом
@itfoxweb/prettier-config - конфиг приттера

# test

jest
ts-jest
@types/jest
